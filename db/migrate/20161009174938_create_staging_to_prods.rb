class CreateStagingToProds < ActiveRecord::Migration
  def change
    create_table :staging_to_prods do |t|

      t.timestamps null: false
    end
  end
end
