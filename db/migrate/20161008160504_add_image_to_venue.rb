class AddImageToVenue < ActiveRecord::Migration
  def self.up
    add_attachment :venues, :avatar
  end

  def self.down
    remove_attachment :venues, :avatar
  end
end
