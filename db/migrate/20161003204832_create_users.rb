class CreateUsers < ActiveRecord::Migration
  def up
    create_table :users do |t|
      t.string :login_type   #fb or gplus
      t.string :login_id  #fb id or gplus profile id
      t.string :uuid, :null => false  #local id to identify user
      t.string :email, :null => false
      t.string :name
      t.string :mobile_number
      t.string :gender
      t.string :min_age
      t.string :dob
      t.string :image_url
      t.boolean :verified
      t.string :occupation
      t.text :remarks
      t.integer :address_id
      t.timestamps 
    end
  end

  def down 
  	drop_table :users
  end
end
