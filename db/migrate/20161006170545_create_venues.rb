class CreateVenues < ActiveRecord::Migration
  def change
    create_table :venues do |t|
      t.string :uuid
      t.string :title
      t.string :description
      t.string :cover_image_url

      t.timestamps null: false
    end
  end
end
