require 'test_helper'

class StagingToProdsControllerTest < ActionController::TestCase
  setup do
    @staging_to_prod = staging_to_prods(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:staging_to_prods)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create staging_to_prod" do
    assert_difference('StagingToProd.count') do
      post :create, staging_to_prod: {  }
    end

    assert_redirected_to staging_to_prod_path(assigns(:staging_to_prod))
  end

  test "should show staging_to_prod" do
    get :show, id: @staging_to_prod
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @staging_to_prod
    assert_response :success
  end

  test "should update staging_to_prod" do
    patch :update, id: @staging_to_prod, staging_to_prod: {  }
    assert_redirected_to staging_to_prod_path(assigns(:staging_to_prod))
  end

  test "should destroy staging_to_prod" do
    assert_difference('StagingToProd.count', -1) do
      delete :destroy, id: @staging_to_prod
    end

    assert_redirected_to staging_to_prods_path
  end
end
