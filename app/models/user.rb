class User < ActiveRecord::Base

	def self.create_new_user (params)
        
        unless is_require_data_available (params) 
            return false, {message: "Email not persent"}  
        end 

        user  = is_user_already_exist(params[:email])

        #User already exit, fetch old data and return        
        unless user.nil?
           return true, get_json_for_display(user)  
        end

        user = User.new(new_user_create_hash (params))        
  
        #save_address(user, params[:address])
        #save_games(user, params[:sport_details])

        if user.save
       	  return true, get_json_for_display(user)
        end
  	end

  	def self.is_user_already_exist (email)
    	return User.find_by_email(email)
  	end

  	def self.is_require_data_available (params)
	    if params.has_key?("email")
	        return true
	    end
	       
	    return false 
 	end

 	def self.new_user_create_hash (params)
        user_hash = Hash.new
        user_hash[:login_type] = params[:login_type]
        user_hash[:login_id] = params[:login_id]
        user_hash[:uuid] = "dummy uuid" 
        user_hash[:email] = params[:email]
        user_hash[:name] = params[:name]
        
        if params[:gender].present?
        	user_hash[:gender] = params[:gender]
        end

        if params[:min_age].present?
        	user_hash[:min_age] = params[:min_age]
        end

        if params[:dob].present?
        	user_hash[:dob] = params[:dob]
        end  

        if params[:remarks].present?
        	user_hash[:remarks] = params[:remarks]
        end

        if params[:image_url].present?
          user_hash[:image_url] = params[:image_url]
        end
        
        if params[:verified].present?
          user_hash[:verified] = params[:verified]
        end

        user_hash.merge(get_user_update_hash(params))
        return user_hash
 	end

def self.get_json_for_display(user)
      if user.nil?
        return nil
      end
      data = user.as_json(except: [:created_at, :updated_at]) 
    
      return data
  end

  def self.get_user_card_detail(user)
      if user.nil?
        return nil
      end
      data = user.as_json(only: ['id', 'uuid', 'name', 'gender', 'min_age', 'dob', 'image_url', 'address_id'])
      return data
   end


  def self.get_user_by_id (id) 
    user = User.find_by_id(id)
    return get_json_for_display(user)
  end

  def self.get_user_list(limit, offset) 
    result = Array.new
    userArray = User.offset(offset).limit(limit).all
    userArray.each do |user|
         result << get_user_card_detail(user) 
    end
    return result
  end

  def self.update_user_info(params) 
     user = find_by_id(params["id"])
     unless user.nil?
        user.update(get_user_update_hash(params))
        if user.save
          return true, get_json_for_display(user)
        end
     end
     return false, nil
  end

  def self.get_user_update_hash (params)
        update_hash = Hash.new
        
        if params[:occupation].present?
          update_hash[:occupation] = params[:occupation]
        end

        if params[:mobile_number].present?
          update_hash[:mobile_number] = params[:mobile_number]
        end
        return update_hash
  end

end
