class Venue < ActiveRecord::Base
  validates :title,  presence: true
  validates :cover_image_url,  presence: true
  validates :description,  presence: true
  # associates attribute :image with a file attachment
  has_attached_file :avatar, {
	    :styles => {
		    thumb: '100x100>',
		    square: '200x200#',
		    medium: '340x340>',
		    large:  '500x500>'
	    },
            #:path =>  "/{image_path}/:id/:style/:filename"
        storage: :s3,
        s3_credentials: {access_key_id: ENV["AWS_ACCESS_KEY_ID"], secret_access_key: ENV["AWS_SECRET_ACCESS_KEY"]},
        bucket: ENV["S3_BUCKET_NAME"]		    
      }

     # Validate the attached image is image/jpg, image/png, etc
     validates_attachment :avatar, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }

      def set_path
      	  image_path = "venues/:id/:filename"
      end
end
