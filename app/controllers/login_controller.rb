class LoginController < ApplicationController

  include LoginHelper

  def create
  	puts params.inspect 
    
    status, data = new_user_login(params)
    render  json: data , status: (status ? :ok : :unauthorized)
  end

  def get
  	puts params.inspect
    if params["id"].present?
       status, data = get_user(params["id"])
       render  json: data , status: (status ? :ok : :unauthorized)
    else 
       render  json: nil, status: :unauthorized
    end
  end
end
