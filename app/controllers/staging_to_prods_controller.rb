class StagingToProdsController < ApplicationController
  before_action :set_staging_to_prod, only: [:show, :edit, :update, :destroy]

  # GET /staging_to_prods
  # GET /staging_to_prods.json
  def index
    @staging_to_prods = StagingToProd.all
  end

  # GET /staging_to_prods/1
  # GET /staging_to_prods/1.json
  def show
  end

  # GET /staging_to_prods/new
  def new
    @staging_to_prod = StagingToProd.new
  end

  # GET /staging_to_prods/1/edit
  def edit
  end

  # POST /staging_to_prods
  # POST /staging_to_prods.json
  def create
    @staging_to_prod = StagingToProd.new(staging_to_prod_params)

    respond_to do |format|
      if @staging_to_prod.save
        format.html { redirect_to @staging_to_prod, notice: 'Staging to prod was successfully created.' }
        format.json { render :show, status: :created, location: @staging_to_prod }
      else
        format.html { render :new }
        format.json { render json: @staging_to_prod.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /staging_to_prods/1
  # PATCH/PUT /staging_to_prods/1.json
  def update
    respond_to do |format|
      if @staging_to_prod.update(staging_to_prod_params)
        format.html { redirect_to @staging_to_prod, notice: 'Staging to prod was successfully updated.' }
        format.json { render :show, status: :ok, location: @staging_to_prod }
      else
        format.html { render :edit }
        format.json { render json: @staging_to_prod.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /staging_to_prods/1
  # DELETE /staging_to_prods/1.json
  def destroy
    @staging_to_prod.destroy
    respond_to do |format|
      format.html { redirect_to staging_to_prods_url, notice: 'Staging to prod was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_staging_to_prod
      @staging_to_prod = StagingToProd.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def staging_to_prod_params
      params.fetch(:staging_to_prod, {})
    end
end
