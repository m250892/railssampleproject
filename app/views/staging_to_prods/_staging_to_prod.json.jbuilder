json.extract! staging_to_prod, :id, :created_at, :updated_at
json.url staging_to_prod_url(staging_to_prod, format: :json)