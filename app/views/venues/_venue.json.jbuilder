json.extract! venue, :id, :uuid, :title, :description, :cover_image_url, :created_at, :updated_at
json.url venue_url(venue, format: :json)