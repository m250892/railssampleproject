module LoginHelper

	def new_user_login (params)
		flag, data = User.create_new_user(params)
		return flag, data
	end

	def get_user(id) 
		data = User.get_user_by_id(id)
		return true, data
	end
end
